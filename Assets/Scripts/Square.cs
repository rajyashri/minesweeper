using UnityEngine;
using System.Collections;

/**
 * @author Anthony Wong
 * @class This class is the minesweeper square. This class
 * is attach to the Square Game Object.
 */ 
public class Square : MonoBehaviour
{
	/**
	 * The list of Sprites 
	 */ 
	public Sprite[] listOfSprites;

	/**
	 * The mine sprite.
	 */ 
	public Sprite mineTexture;	
	
	/**
	 * The square is a mine.
	 */ 
	public bool isMine;

	/**
	 * The flag sprite;
	 */
	public Sprite mineFlag;

	/**
	 * The mine is flagged; 
	 */
	public bool isFlagged;

	/**
	 * When the square is created. 
	 */ 
	void Awake(){
		this.isMine = Random.value < 0.40;

		int x = (int)transform.position.x;
		int y = (int)transform.position.y;
		Grid.GRID [x, y] = this;
	}
	void OnMouseOver(){
		if (Input.GetMouseButtonDown(1)) {
			flagSprite();	
		}
		if (Input.GetMouseButtonDown (0)) {
			leftClick ();
		}

	}
	/**
	 * When the square is clicked.
	 */ 
	void leftClick() {

		if (isMine) {
			GetComponent<SpriteRenderer> ().sprite = mineTexture;

			// Checks to see if the Law Players lost
			Grid.numberOfMinesHIT++;

			// Game Over
			if (Grid.MINES_HIT_LIMIT == Grid.numberOfMinesHIT) {
				ChangeScene.ChangeToStatic ("Bad Ending");
			}

		} else {
			int x = (int)transform.position.x;
			int y = (int)transform.position.y;
			//loadSprite(Grid.adjacentMines(x, y));

			// uncover area without mines
			Grid.FFuncover(x, y, new bool[Grid.WIDTH, Grid.HEIGHT]);

			if (Grid.isFinished()){
				ChangeScene.ChangeToStatic("Good Ending");
			}
		}
	}

	/**
	 * Check to see if the current square uses the default sprite.
	 */ 
	public bool isCovered(){
		return GetComponent<SpriteRenderer> ().sprite.texture.name == "default";
	}

	/**
	 * Load different sprite based on the value.
	 */ 
	public void loadSprite(int adjacentCount){
		if (isMine) {
			GetComponent<SpriteRenderer>().sprite = mineTexture;
		} else {
			GetComponent<SpriteRenderer>().sprite = listOfSprites[adjacentCount];
			//GetComponent<SpriteRenderer>().sprite = listOfSprites[0];
		}
	}
	public void flagSprite(){
		GetComponent<SpriteRenderer> ().sprite = mineFlag;
		isFlagged = true;
		if (isMine)
			Grid.numberOfMinesFlagged += 1;  
	}
}